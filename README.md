# Project Title
__Description__

## Setup
Make sure [Nodejs][] and [npm][] are installed and updated
Duplicate config.json.local into config.json file and change the main settings.

Server settings are in the browserSync part in config.json. Remove the proxy part if you want to init a local server with browserSync. Remove the server part if you want to use an external server and specify the proxy address.

Clone repo, install dependencies and launch a server.
```
git clone git@bitbucket.org:Satche/REPO.git
cd REPO
npm install
npm start
```

## Commands
* `npm start` display a prompt, you can set up the environnement and start a server.
* `npm run dev` start a server/proxy in a developement environnement.
* `npm run prod` run a compilation in the dist folder for production.

## Authors
* **Thomas Robert** - *Web design, front-end development* - [Github](https://github.com/Satche), [Bitbucket](https://bitbucket.org/Satche/), [Website](http://satche.ch)

[//]: # ( LINKS REFERENCES EXEMPLES )

[Nodejs]: https://nodejs.org "Nodejs: JS runtime platform"
[npm]: https://www.npmjs.com/ "Nodejs Package Manager"
